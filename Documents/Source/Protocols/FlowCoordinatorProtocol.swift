//
//  FlowCoordinatorProtocol.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit

protocol FlowCoordinatorProtocol: class {
    
    var navigationController: UINavigationController { get }
    
    func start()
}
