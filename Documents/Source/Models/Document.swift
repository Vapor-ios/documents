//
//  Document.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import Decodable
import protocol Decodable.Decodable

struct Document {
    let id: Int
    let title: String
    let category: String
    let thumbnailImageURLString: String
    let headerImageURLString: String
    let author: String
    let createdDate: Date
    let text: String // HTML
    
    init(id: Int, title: String, category: String, thumbnailImageURLString: String,
         headerImageURLString: String, author: String, createdDate: Date, text: String) {
        self.id = id
        self.title = title
        self.category = category
        self.thumbnailImageURLString = thumbnailImageURLString
        self.headerImageURLString = headerImageURLString
        self.author = author
        self.createdDate = createdDate
        self.text = text
    }
}

extension Document: Decodable {
    static func decode(_ j: Any) throws -> Document {
        do {
            return try Document(id: j => "id",
                                title: j => "title",
                                category: j => "category",
                                thumbnailImageURLString: j => "thumbnailImg",
                                headerImageURLString: j => "headerImg",
                                author: j => "author",
                                createdDate: j => "created",
                                text: j => "text")
        } catch {
            log.error("Could not decode Document JSON: \(error)")
            throw error
        }
    }
}
