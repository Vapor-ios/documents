//
//  DocumentList.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import Decodable
import protocol Decodable.Decodable

struct DocumentList {
    let documents: [DocumentInfo]
}

extension DocumentList: Decodable {
    static func decode(_ j: Any) throws -> DocumentList {
        do {
            return try DocumentList(documents: j => "documents")
        } catch {
            log.error("Could not decode DocumentList JSON: \(error)")
            throw error
        }
    }
}
