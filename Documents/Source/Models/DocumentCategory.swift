//
//  DocumentCategory.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation

struct DocumentCategory {
    let name: String
    let documents: [DocumentInfo]
}

extension Array where Element == DocumentCategory {
    
    /// Sort documents into DocumentCategory objects
    init(from infos: [DocumentInfo]) {
        let sortedDocs = infos.sorted { doc1, doc2 in
            return doc1.category < doc2.category
        }
        
        var result = [DocumentCategory]()
        var currentCategoryName = ""
        var currentDocs = [DocumentInfo]()
        
        for doc in sortedDocs {
            if currentCategoryName != doc.category {
                if currentDocs.count > 0 {
                    result.append(DocumentCategory(name: currentCategoryName, documents: currentDocs))
                }
                
                currentCategoryName = doc.category
                currentDocs = []
            }
            
            currentDocs.append(doc)
        }
        
        if currentDocs.count > 0 {
            result.append(DocumentCategory(name: currentCategoryName, documents: currentDocs))
        }
        
        self = result
    }
}
