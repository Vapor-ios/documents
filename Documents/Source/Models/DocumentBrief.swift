//
//  DocumentBrief.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import Decodable
import protocol Decodable.Decodable

struct DocumentInfo {
    let id: Int
    let title: String
    let category: String
    let thumbnailImageURLString: String
    
    init(id: Int, title: String, category: String, thumbnailImageURLString: String) {
        self.id = id
        self.title = title
        self.category = category
        self.thumbnailImageURLString = thumbnailImageURLString
    }
}

extension DocumentInfo: Decodable {
    static func decode(_ j: Any) throws -> DocumentInfo {
        do {
            return try DocumentInfo(id: j => "id",
                                     title: j => "title",
                                     category: j => "category",
                                     thumbnailImageURLString: j => "thumbnailImg")
        } catch {
            log.error("Could not decode DocumentBrief JSON: \(error)")
            throw error
        }
    }
}
