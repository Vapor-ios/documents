//
//  AppDelegate.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit
import SwiftyBeaver
import Decodable
import protocol Decodable.Decodable

let log = SwiftyBeaver.self

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: FlowCoordinatorProtocol?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Date.decoder = Date.decoder(using: DecodableDate.formatter)
        
        // Start the app
        let wnd = UIWindow(frame: UIScreen.main.bounds)
        self.window = wnd
        
        let coord = ListFlowCoordinator(dataProvider: DataProvider())
        self.coordinator = coord
        
        wnd.rootViewController = coord.navigationController
        wnd.makeKeyAndVisible()
        coord.start()
        
        return true
    }
}

