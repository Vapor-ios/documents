//
//  ListViewController.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ListViewController: UITableViewController, ViewControllerProtocol {

    let viewModel: ListViewModel

    private let disposeBag = DisposeBag()
    
    init(viewModel: ListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Initializer not supported")
    }
    
    override func viewDidLoad() {
        setup()
        bindViewModel()
    }

    // MARK: Private
    
    private func setup() {
        title = "Documents list"
        
        tableView.register(UINib(nibName: String(describing: ListCell.self), bundle: nil),
                           forCellReuseIdentifier: String(describing: ListCell.self))
    }
    
    private func bindViewModel() {
        viewModel.dataSource
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
            self?.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
}

extension ListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.dataSource.value.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.value[section].documents.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let document = viewModel.dataSource.value[indexPath.section].documents[indexPath.row]
        let model = ListCell.Model(title: document.title,
                                   thumbnailImageURL: URL(string: document.thumbnailImageURLString))
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ListCell.self), for: indexPath) as! ListCell
        cell.setup(with: model)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.dataSource.value[section].name
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didTapDocument(atIndexPath: indexPath)
    }
}
