//
//  DetailsViewModel.swift
//  
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//

import Foundation

final class DetailsViewModel: ViewModelProtocol {
    
    let imageURL: URL?
    let title: String
    let author: String
    let htmlText: NSAttributedString
    
    init(document: Document) {
        self.imageURL = URL(string: document.headerImageURLString)
        self.title = document.title
        self.author = document.author
        
        let fontPrefix = "<style>body{font-family: -apple-system; font-size:16px;}</style>"
        let text = "\(fontPrefix)\(document.text)"
        let htmlText = (try? NSAttributedString(htmlString: text)) ?? NSAttributedString()
        self.htmlText = htmlText
    }
}
