//
//  ListCell.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit
import Kingfisher

final class ListCell: UITableViewCell {
    
    struct Model {
        let title: String
        let thumbnailImageURL: URL?
    }
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(with model: Model) {
        if let url = model.thumbnailImageURL {
            thumbnailImageView.kf.indicatorType = .activity
            thumbnailImageView.kf.setImage(with: url)
            thumbnailImageView.alpha = 1
        } else {
            thumbnailImageView.alpha = 0
        }
        
        titleLabel.text = model.title
    }
}
