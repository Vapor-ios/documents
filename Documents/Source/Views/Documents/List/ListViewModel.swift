//
//  ListViewModel.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol ListViewModelDelegate: class {
    func listDidTapDocument(_ document: Document)
}

final class ListViewModel: ViewModelProtocol {
    
    weak var delegate: ListViewModelDelegate?
    
    let dataProvider: DataProviderProtocol
    let dataSource = BehaviorRelay<[DocumentCategory]>(value: [])
    
    private let disposeBag = DisposeBag()
    
    init(dataProvider: DataProviderProtocol) {
        self.dataProvider = dataProvider
        
        fetchDocumentsInfos()
    }
    
    func fetchDocumentsInfos() {
        Loading.show()
        dataProvider.requestDocumentList().asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] list in
                let docs = [DocumentCategory](from: list.documents)
                self?.dataSource.accept(docs)
                Loading.hide()
                }, onError: { error in
                    log.error("Error in (\(#function)): \(error)")
                    Loading.hide()
            }).disposed(by: disposeBag)
    }
    
    func didTapDocument(atIndexPath indexPath: IndexPath) {
        let docInfo = dataSource.value[indexPath.section].documents[indexPath.row]
        let id = docInfo.id
        
        Loading.show()
        dataProvider.requestDocument(withID: id).asObservable()
            .subscribe(onNext: { [weak self] doc in
                self?.delegate?.listDidTapDocument(doc)
                Loading.hide()
                }, onError: { error in
                    log.error("Error in (\(#function)): \(error)")
                    Loading.hide()
            }).disposed(by: disposeBag)
    }
}
