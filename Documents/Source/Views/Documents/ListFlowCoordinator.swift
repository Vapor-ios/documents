//
//  ListFlowCoordinator.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit

final class ListFlowCoordinator: FlowCoordinatorProtocol {
    
    let navigationController = UINavigationController()
    let dataProvider: DataProviderProtocol
    
    init(dataProvider: DataProviderProtocol) {
        self.dataProvider = dataProvider
    }
    
    func start() {
        navigationController.navigationBar.tintColor = .darkText
        
        let vm = ListViewModel(dataProvider: dataProvider)
        vm.delegate = self
        let vc = ListViewController(viewModel: vm)
        navigationController.pushViewController(vc, animated: false)
    }
    
    func showDetails(for document: Document) {
        let vm = DetailsViewModel(document: document)
        let vc = DetailsViewController(viewModel: vm)
        navigationController.pushViewController(vc, animated: true)
    }
}

extension ListFlowCoordinator: ListViewModelDelegate {
    func listDidTapDocument(_ document: Document) {
        showDetails(for: document)
    }
}
