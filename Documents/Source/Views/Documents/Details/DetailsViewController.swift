//
//  DetailsViewController.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit
import Kingfisher

final class DetailsViewController: UIViewController, ViewControllerProtocol {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    let viewModel: DetailsViewModel
    
    init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Coding initializer not supported")
    }
    
    override func viewDidLoad() {
        setup()
    }
    
    // MARK: Private
    
    private func setup() {
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: viewModel.imageURL)
        titleLabel.text = viewModel.title
        authorLabel.text = viewModel.author
        textView.attributedText = viewModel.htmlText
    }
}
