//
//  Moya+.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import Decodable
import protocol Decodable.Decodable

extension ObservableType where E == Response {
    public func mapObject<T: Decodable>(_ type: T.Type) -> Observable<T> {
        return observeOn(SerialDispatchQueueScheduler(qos: DispatchQoS.background))
            .flatMap{ (response) -> Observable<T> in
                return Observable.just(try response.mapObject())
            }
            .observeOn(MainScheduler.instance)
    }
    
    func mapArray<T: Decodable>(_ type: T.Type) -> Observable<[T]> {
        return observeOn(SerialDispatchQueueScheduler(qos: DispatchQoS.background))
            .flatMap{ (response) -> Observable<[T]> in
                return Observable.just(try response.mapObjectArray())
            }
            .observeOn(MainScheduler.instance)
    }
}

extension Response {
    func mapObject<T: Decodable>() throws -> T {
        do {
            return try T.decode(try mapJSON())
        } catch {
            throw error
        }
    }
    
    func mapObjectArray<T: Decodable>() throws -> [T] {
        let jsonObject: Any
        
        do {
            jsonObject = try mapJSON()
        } catch {
            log.error("Map array failed. JSON: \(data)")
            throw MoyaError.jsonMapping(self)
        }
        
        guard let json = jsonObject as? NSArray else {
            log.error("JSON is not an array! \(jsonObject)")
            throw MoyaError.jsonMapping(self)
        }
        
        do {
            return try [T].decode(json)
        } catch {
            throw error
        }
    }
}
