//
//  API.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import Moya

enum API {
    case documents
    case document(id: Int)
}

extension API: TargetType {
    var baseURL: URL {
        return URL(string: "http://localhost:8080/api/v1")!
    }
    
    var path: String {
        switch self {
        case .documents: return "/documents"
        case .document(let id): return "/documents/\(id)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .documents,
             .document:
            return .get
        }
    }
    
    var sampleData: Data {
        fatalError("API not ready for testing")
    }
    
    var task: Task {
        switch self {
        case .documents,
             .document:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? { return nil }
}
