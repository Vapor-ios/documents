//
//  DataProvider.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Alamofire

final class DataProvider: DataProviderProtocol {
    
    let provider = MoyaProvider<API>()
    
    func requestDocumentList() -> Observable<DocumentList> {
        log.verbose("[DataProvider] Request issued: \(#function)")
        return provider.rx.request(.documents)
            .asObservable()
            .mapObject(DocumentList.self)
    }
    
    func requestDocument(withID id: Int) -> Observable<Document> {
        log.verbose("[DataProvider] Request issued: \(#function)")
        return provider.rx.request(.document(id: id))
            .asObservable()
            .mapObject(Document.self)
    }
}
