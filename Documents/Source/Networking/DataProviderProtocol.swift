//
//  DataProviderProtocol.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation
import RxSwift

protocol DataProviderProtocol: class {
    func requestDocumentList() -> Observable<DocumentList>
    func requestDocument(withID id: Int) -> Observable<Document>
}
