//
//  Loading.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import UIKit

final class Loading {
    
    private static var window: UIWindow?
    
    static func show() {
        guard window == nil else { return }
        
        let wnd = UIWindow(frame: UIScreen.main.bounds)
        self.window = wnd
        
        let vc = LoadingViewController()
        wnd.rootViewController = vc
        wnd.windowLevel = .alert
        wnd.makeKeyAndVisible()
    }
    
    static func hide() {
        guard window != nil else { return }
        window?.isHidden = true
        window = nil
    }
}

private final class LoadingViewController: UIViewController, ViewControllerProtocol {
    override func viewDidLoad() {
        view.backgroundColor = .clear
        
        let loadingView = UIView()
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loadingView)
        loadingView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        loadingView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        loadingView.layer.cornerRadius = 8.0
        
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        loadingView.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
        indicator.startAnimating()
    }
}
