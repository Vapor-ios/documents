//
//  DecodableDate.swift
//  Documents
//
//  Created by Paweł Wojtkowiak on 05/12/2018.
//  Copyright © 2018 Paweł Wojtkowiak. All rights reserved.
//

import Foundation

final class DecodableDate {
    static var formatter: DateFormatter = {
        let formatter = DateFormatter()
        
        // WORKAROUND to ignore device configuration regarding AM/PM http://openradar.appspot.com/radar?id=1110403
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        // translate to Gregorian calendar if other calendar is selected in system settings
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        gregorian.timeZone = TimeZone(abbreviation: "GMT")!
        formatter.calendar = gregorian
        return formatter
    }()
}
